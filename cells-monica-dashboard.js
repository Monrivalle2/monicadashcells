{
  const {
    html,
  } = Polymer;
  /**
    `<cells-monica-dashboard>` Description.

    Example:

    ```html
    <cells-monica-dashboard></cells-monica-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-monica-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsMonicaDashboard extends Polymer.Element {

    static get is() {
      return 'cells-monica-dashboard';
    }

    static get properties() {
      return {
        paso:{
            type: Boolean,
            value: false
        },
        alumnosArry : {
            type : Object,
            value : {},
            notify : true
        }
    };
  }
  
  validarPase(e){
            console.log(e.detail.value);
            if(e.detail.value === true){
                this.set('paso',e.detail.value);
                console.log(e.detail.value);
               
            }else{
                console.log('No paso');
            }
        }
        salirPage(){
            this.paso = false;
        }
        filtrar(string) {
        if (!string) {
          //establece filtro en nulo para deshabilitar el filtrado
          return null;
        } else {
          // devuelve una función de filtro para la cadena de búsqueda actual
          string = string.toLowerCase();
          return function(alumnosArry) {
            var name = alumnosArry.name.toLowerCase();
            var last = alumnosArry.last.toLowerCase();
            return (name.indexOf(string) != -1 ||  last.indexOf(string) != -1);
          };
        }
      }
    static get template() {
      return html `
      <style include="cells-monica-dashboard-styles cells-monica-dashboard-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>
        
        <template is="dom-if" if="[[paso]]">
                <div><p>PASSOO</div> 
            </div>
        </template>

        <template is="dom-if" if="[[!paso]]">
        <iron-ajax 
                auto 
                url="https://api.chucknorris.io/jokes/random" 
                handle-as="json" 
                last-response="{{ajaxResponse}}"> 
        </iron-ajax>

        <div class="card" id="vistaGeneral">
            <div class="circle">M</div><button style="float: right;" on-click="salirPage">Salir</button>
                <div align="center">
                    <h1>Vista Mónica</h1>
                    <img src="../images/mon.png">
                    <p><b>Nombre:</b> Mónica Rivera Valle</p>
                    <p><b>Dirección:</b> Chimalhuacán, Estado de México</p>
                    <p><b>Hobbie:</b> Ver series, películas</p>
                </div>
            </div>

            <div class="card" align="center">
                <b>API Chuck Norris</b><br />
                <img src="{{ajaxResponse.icon_url}}"><br />
                [[ajaxResponse.value]]
                
            </div>
           
            <div class="card" >
            <h2>Lista de alumnos</h2>
            <cells-mocks-component alumnos="{{alumnosArry}}"></cells-mocks-component>
 
              <input type="text" placeholder="Filtro" value="{{buscarCadena::input}}">
               <template is="dom-repeat" items="{{alumnosArry}}" 
                filter="{{filtrar(buscarCadena)}}">

                    <div align="center">
                        <img src="{{item.img}}"width="80" height="80">
                        <p>Nombre: <span>{{item.name}}</span></p>
                        <p>Apellido: <span>{{item.last}}</span></p> 
                        <p>Dirección: <span>{{item.address}}</span></p> 
                        <p>Hobbies: <span>{{item.hobbies}}</span></p>
                    </div>  
                </template>
        </div>
        
      `;
    }
  }

  customElements.define(CellsMonicaDashboard.is, CellsMonicaDashboard);
}